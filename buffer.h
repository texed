
#ifndef BUFFER_H
#define BUFFER_H

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define LT 0
#define RT 1
#define UP 2
#define DN 3

#define NEXT 0
#define PREV 1

typedef struct
{
  int x;
  int y;
} position_t;

typedef struct
{
  char* buf;
  unsigned int size;
  int gap_start;
  int gap_end;
  int* line_indices;
  int num_lines;
  char* name;
  position_t cursor;
} buffer_t;

buffer_t buffer_init();

buffer_t* buffer_init_point();

void buffer_free (buffer_t* buffer);

void buffer_inspect (buffer_t buffer);

void buffer_print (buffer_t buffer);

void buffer_grow (buffer_t* buffer, unsigned int add);

void buffer_insert_char (buffer_t* buffer, char new);

void buffer_backspace (buffer_t* buffer);

void buffer_left (buffer_t* buffer);

void buffer_right (buffer_t* buffer);

void buffer_up (buffer_t* buffer);

void buffer_down (buffer_t* buffer);

void buffer_set_contents (buffer_t* buffer, char string[]);

char* buffer_to_string (buffer_t buffer);

void buffer_jump_word (buffer_t* buffer, char delim, int dir);

#endif
