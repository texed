
FILES= guile_wrap.c buffer.c display.c main.c

CFLAGS= -Wall -Wextra -lncurses \
	`guile-config compile`  \
	`guile-config link`

# buffer:
# 	gcc -fPIC --shared buffer.c -o buffer.so

all:
	gcc ${FILES} ${CFLAGS} -o texed

