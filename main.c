
#include <libguile.h>
#include "guile_wrap.h"

int main(int argc, char *argv[])
{
  /* int state_flag = NORMAL_STATE; */

  scm_with_guile(&register_functions, NULL);
  /* scm_shell(argc, argv); */
  scm_c_primitive_load("display.scm");
  
  return 0;
}
