
#ifndef DISPLAY_H
#define DISPLAY_H

#define NORMAL_STATE 0
#define INSERT_STATE 1
#define VISUAL_STATE 2

#include "buffer.h"
#include <ncurses.h>

typedef struct
{
  WINDOW buffer_display;
  WINDOW modeline;
  buffer_t contents;
} editor_window_t;

WINDOW* main_window;

WINDOW* minibuf;

void display_init ();

void minibuf_echo (char* string);

void minibuf_clear ();

void cursor_move_handler (buffer_t* current, int key);

void display_render (buffer_t contents, int state);

#endif
