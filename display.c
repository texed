
#include "display.h"

void display_init ()
{
  initscr();
  raw();
  noecho();
  keypad(stdscr, TRUE);

  refresh();
  main_window = newwin(LINES - 1, COLS, 0, 0);
  wrefresh(main_window);
  minibuf = newwin(1, COLS, LINES - 1, 0);
  wrefresh(minibuf);
  refresh();
}

void minibuf_echo (char* string)
{
  wclear(minibuf);
  wprintw(minibuf, string);
  /* wrefresh(minibuf); */
}

void minibuf_clear ()
{
  wclear(minibuf);
  /* wrefresh(minibuf); */
}

void display_render (buffer_t contents, int state)
{
  wclear(main_window);
  wclear(minibuf);
  wprintw(main_window, buffer_to_string(contents));
  wprintw(minibuf, "%d:%d ",
	  contents.cursor.y, contents.cursor.x);
  switch (state)
    {
      case NORMAL_STATE:
	wprintw(minibuf, "<N>\n");
	break;
      case INSERT_STATE:
	wprintw(minibuf, "<I>\n");
	break;
    }
  /* wmove(main_window, contents.cursor.y, contents.cursor.x); */
  move(contents.cursor.y, contents.cursor.x);
  wrefresh(main_window);
  wrefresh(minibuf);
  /* refresh(); */
}

void cursor_move_handler (buffer_t* current, int key)
{
  switch (key)
    {
      case 'k': buffer_up(current);
	break;
      case 'j': buffer_down(current);
	break;
      case 'h': buffer_left(current);
	break;
      case 'l': buffer_right(current);
	break;
    }
}

