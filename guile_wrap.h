
#ifndef GUILE_WRAP_H
#define GUILE_WRAP_H

#include <libguile.h>

SCM buffer_init_wrap ();

SCM buffer_print_wrap (SCM buffer_obj);

void register_functions ();

#endif
