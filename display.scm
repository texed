#!/usr/bin/guile --listen
!#

(use-modules (ncurses curses)
	     (srfi    srfi-9)
	     (system repl server))

(define app (make-tcp-server-socket #:port 37146))

(spawn-server app)

(define-record-type <subeditor>
  (make-subeditor main-display modeline contents)
  subeditor?
  (main-display subeditor-main set-subeditor-main!)
  (modeline subeditor-modeline set-subeditor-modeline!)
  (contents sub-editor-contents set-subeditor-contents!))

(define stdscr (initscr))

(define (__display-init)
  (begin
    (cbreak!)
    (noecho!)
    (nonl!)
    (intrflush! #f)
    (keypad! stdscr #t)))

(__display-init)

(addstr stdscr "Hello world")

(refresh stdscr)

(getch stdscr)

(endwin)

(stop-server-and-clients!)
