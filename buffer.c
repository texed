
#include "buffer.h"

buffer_t buffer_init()
{
  char* buffer = calloc(sizeof(char), 80); // start at 80 chars per line
  int* indices = calloc(sizeof(int), 40);

  if (!buffer)
    {
      fputs("Out of memory, buffer_init()\n", stderr);
      exit(1);
    }

  return (buffer_t)
    {
      .buf          = buffer,
      .size         = 80,
      .gap_start    = 0,
      .gap_end      = 79,
      .line_indices = indices,
      .num_lines    = 1,
      .name         = "untitled",
      .cursor       = (position_t) {0,0}
    };
}

buffer_t* buffer_init_point ()
{
  buffer_t* result = calloc(sizeof(buffer_t), 1);
  char* buffer = calloc(sizeof(char), 80); // start at 80 chars per line
  int* indices = calloc(sizeof(int), 40);

  if (!buffer || !result)
    {
      fputs("Out of memory, buffer_init()\n", stderr);
      exit(1);
    }

  result->buf          = buffer;
  result->size         = 80;
  result->gap_start    = 0;
  result->gap_end      = 79;
  result->line_indices = indices;
  result->num_lines    = 1;
  result->name         = "untitled";
  result->cursor       = (position_t) {0,0};

  return result;
}

void buffer_free (buffer_t* buffer)
{
  free(buffer->buf);
  buffer->size = 0;
  buffer->gap_start = 0;
  buffer->gap_end = 0;
}

void buffer_inspect (buffer_t buffer)
{
  printf("BUFFER DETAILS\n"
	 "--------------\n"
	 "NAME        : %s\n"
	 "SIZE        : %d\n"
	 "GAP_START   : INDEX %d\n"
	 "GAP_END     : INDEX %d\n"
	 "NUM_LINES   : %d\n"
	 "CURSOR POS  : y(%d), x(%d)\n"
	 "LINE INDICES: [",
	 buffer.name, buffer.size,
	 buffer.gap_start, buffer.gap_end,
	 buffer.num_lines, buffer.cursor.y,
	 buffer.cursor.x);

  for (int i = 0; i < buffer.num_lines; i++)
    {
      printf("%d, ", buffer.line_indices[i]);
    }

  puts("]\n\n"
       "BUFFER CONTENTS\n"
       "---------------\n");

  for (int i = 0; i < (int) buffer.size; i++)
    {
      if (buffer.buf[i] == '\n')
	puts("\\n, ");
      else if (buffer.buf[i] == '\0')
	printf("\\0, ");
      else
	printf("%c, ", buffer.buf[i]);
    }
  puts("\n");
}

void buffer_print (buffer_t buffer)
{
  char* head = calloc(sizeof(char), buffer.gap_start + 1);
  char* tail = calloc(sizeof(char), (buffer.size - buffer.gap_end) + 1);

  for (int i = 0; i < buffer.gap_start; i++)
    {
      head[i] = buffer.buf[i];
    }

  int count = 0;
  for (int i = buffer.gap_end + 1; i < (int) buffer.size; i++)
    {
      tail[count] = buffer.buf[i];
      count++;
    }

  printf("BUFFER DETAILS\n"
	 "--------------\n"
	 "NAME        : %s\n"
	 "SIZE        : %d\n"
	 "GAP_START   : INDEX %d\n"
	 "GAP_END     : INDEX %d\n"
	 "NUM_LINES   : %d\n"
	 "CURSOR POS  : y(%d), x(%d)\n"
	 "LINE INDICES: [",
	 buffer.name, buffer.size,
	 buffer.gap_start, buffer.gap_end,
	 buffer.num_lines, buffer.cursor.y,
	 buffer.cursor.x);

  for (int i = 0; i < buffer.num_lines; i++)
    {
      printf("%d, ", buffer.line_indices[i]);
    }

  printf("]\n\n"
         "BUFFER CONTENTS\n"
         "---------------\n"
	 "%s<C>%s\n\n", head, tail);

  free(head);
  free(tail);
}

void buffer_grow (buffer_t* buffer, unsigned int add)
{
  char* new_buf = calloc(sizeof(char), buffer->size + add);
  int* new_indices = calloc(sizeof(int), buffer->size);
  int new_end = buffer->gap_end + add;
  int new_size = buffer->size + add;

  for (int i = 0; i < buffer->gap_start; i++)
    new_buf[i] = buffer->buf[i];
    
  for (int i = new_end; i < new_size; i++)
    new_buf[i] = buffer->buf[i - add];

  for (int i = 0; i < buffer->num_lines; i++)
    new_indices[i] = buffer->line_indices[i];

  free(buffer->buf);
  free(buffer->line_indices);
  buffer->buf = new_buf;
  buffer->line_indices = new_indices;
  buffer->gap_end = new_end;
  buffer->size = new_size;
}


void buffer_insert_char (buffer_t* buffer, char new)
{
  int gap_length = buffer->gap_end - buffer->gap_start;

  if (gap_length < 5)
    buffer_grow(buffer, buffer->size + buffer->size);

  buffer->buf[buffer->gap_start] = new;
  if (new == '\n')
    {
      buffer->line_indices[buffer->num_lines] = buffer->gap_start;
      buffer->cursor.x = 0;
      buffer->cursor.y++;
      buffer->num_lines++;
    }
  else
    {
      /* buffer->line_indices[buffer->cursor.y + 1]++; */
      buffer->cursor.x++;
    }

  buffer->gap_start++;
}

void buffer_backspace (buffer_t* buffer)
{
  char to_erase = buffer->buf[buffer->gap_start - 1];
  if (to_erase == '\n')
    {
      int current_index = buffer->line_indices[buffer->cursor.y];
      int x_pos =  buffer->gap_start - current_index;
      buffer->cursor.y--;
      buffer->cursor.x = x_pos;
    }
  else
    buffer->cursor.x--;
  
  buffer->buf[buffer->gap_start - 1] = '\0';
  buffer->gap_start--;
}

void buffer_left (buffer_t* buffer)
{
  if (buffer->gap_start == 0)
    return;

  char to_move = buffer->buf[buffer->gap_start - 1];
  if (to_move == '\n')
    {
      int current_line_index = buffer->line_indices[buffer->cursor.y];
      int prev_line_index = buffer->line_indices[buffer->cursor.y - 1];
      unsigned int prev_line_length = current_line_index - prev_line_index;
      buffer->cursor.y--;
      buffer->cursor.x = (int) prev_line_length - 1;
    }
  else
    buffer->cursor.x--;

  buffer->buf[buffer->gap_end] = to_move;
  buffer->buf[buffer->gap_start - 1] = '\0';

  buffer->gap_start--;
  buffer->gap_end--;
}

void buffer_right (buffer_t* buffer)
{
  if (buffer->gap_end == (int) buffer->size - 1)
    return;
  
  char to_move = buffer->buf[buffer->gap_end + 1];
  if (to_move == '\n')
    {
      buffer->cursor.x = 0;
      buffer->cursor.y++;
    }
  else
    buffer->cursor.x++;

  buffer->buf[buffer->gap_start] = to_move;
  buffer->buf[buffer->gap_end + 1] = '\0';

  buffer->gap_start++;
  buffer->gap_end++;
}

void buffer_up (buffer_t* buffer)
{
  int x_pos = buffer->cursor.x;
  int current_line_index = buffer->line_indices[buffer->cursor.y];

  if (buffer->cursor.y == 0)
    return;

  int prev_line_index = buffer->line_indices[buffer->cursor.y - 1];
  int prev_line_length = current_line_index - prev_line_index;

  if (x_pos > prev_line_length)
    {
      for (int i = 0; i < (x_pos + 1); i++)
	buffer_left(buffer);
    }
  else
    {
      int to_move = prev_line_length;
      for (int i = 0; i < to_move; i++)
	buffer_left(buffer);
    }
}

void buffer_down (buffer_t* buffer)
{
  int x_pos = buffer->cursor.x;

  int next_line_start = buffer->line_indices[buffer->cursor.y + 1];
  int next_line_end = buffer->line_indices[buffer->cursor.y + 2];
  int next_line_length = next_line_end - next_line_start;

  int current_line_index = buffer->line_indices[buffer->cursor.y];
  int current_line_length = next_line_start - current_line_index;

  int to_move = 0;
  if (!next_line_start || !next_line_end)
    {
      int gap_length   = buffer->gap_end - buffer->gap_start;
      int occupied     = buffer->size - gap_length;
      next_line_length = occupied - next_line_start;
    }

  if (x_pos > next_line_length)         //account for zero-indexing
    to_move = (current_line_length - x_pos) + next_line_length - 1;

  else
    to_move = current_line_length;
    
  for (int i = 0; i < to_move; i++)
    buffer_right(buffer);
}

void buffer_set_contents (buffer_t* buffer, char string[])
{
  int length = strlen(string);
  for (int i = 0; i < length; i++)
    buffer_insert_char(buffer, string[i]);
}

char* buffer_to_string (buffer_t buffer)
{
  int occupied = buffer.size - (buffer.gap_end - buffer.gap_start);
  char* head = calloc(sizeof(char), buffer.gap_start + 1);
  char* tail = calloc(sizeof(char), (buffer.size - buffer.gap_end) + 1);
  char* result = calloc(sizeof(char), occupied + 1);

  for (int i = 0; i < buffer.gap_start; i++)
    {
      head[i] = buffer.buf[i];
    }

  int count = 0;
  for (int i = buffer.gap_end + 1; i < (int) buffer.size; i++)
    {
      tail[count] = buffer.buf[i];
      count++;
    }

  strcat(result, head);
  strcat(result, tail);
  free(head);
  free(tail);

  return result;
}

void buffer_jump_word (buffer_t* buffer, char delim, int dir)
{
  int quit = 0;
  char to_move;

  while (!quit)
    {
      if (dir == 0)
	{
	  to_move = buffer->buf[buffer->gap_end + 1];
	  buffer_right(buffer);
	}
      else 
	{
	  to_move = buffer->buf[buffer->gap_start - 1];
	  buffer_left(buffer);
	}

      if ((to_move == delim) ||
	  (buffer->gap_start == 0) ||
	  (buffer->gap_end == (int) (buffer->size - 1)))
	quit = 1;
    }
}
