
#include "buffer.h"
#include "display.h"
#include "guile_wrap.h"

SCM buffer_init_wrap ()
{
  scm_t_pointer_finalizer f = buffer_free;
  return scm_from_pointer(buffer_init_point(), f);
}

SCM buffer_print_wrap (SCM buffer_obj)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  buffer_print(*buf);
  return SCM_UNSPECIFIED;
}

SCM buffer_inspect_wrap (SCM buffer_obj)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  buffer_inspect(*buf);
  return SCM_UNSPECIFIED;
}

SCM buffer_insert_wrap (SCM buffer_obj, SCM ch)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  char ch_unwrap = (char) SCM_CHAR(ch);
  buffer_insert_char(buf, ch_unwrap);
  return SCM_UNSPECIFIED;
}

SCM buffer_backspace_wrap (SCM buffer_obj)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  buffer_backspace(buf);

  return SCM_UNSPECIFIED;
}

SCM buffer_set_wrap (SCM buffer_obj, SCM string)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  char* string_unwrap = scm_to_locale_string(string);
  buffer_set_contents(buf, string_unwrap);

  return SCM_UNSPECIFIED;
}

SCM buffer_cursor_wrap (SCM buffer_obj, SCM dir)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  char* dir_unwrap = scm_to_locale_string(dir);
  char direction = dir_unwrap[0];

  switch (direction)
    {
      case 'u': buffer_up(buf);
	break;
      case 'd': buffer_down(buf); 
	break;
      case 'l': buffer_left(buf); 
	break;
      case 'r': buffer_right(buf);
	break;
    }

  return SCM_UNSPECIFIED;
}

SCM buffer_string_wrap (SCM buffer_obj)
{
  buffer_t* buf = scm_to_pointer(buffer_obj);
  char* result = buffer_to_string(*buf);
  return scm_from_locale_string(result);
}

void register_functions (void* data)
{
  scm_c_define_gsubr("buffer-init", 0, 0, 0, &buffer_init_wrap);
  scm_c_define_gsubr("buffer-print", 1, 0, 0, &buffer_print_wrap);
  scm_c_define_gsubr("buffer-inspect", 1, 0, 0, &buffer_inspect_wrap);
  scm_c_define_gsubr("buffer-insert-char!", 2, 0, 0, &buffer_insert_wrap);
  scm_c_define_gsubr("buffer-backspace!", 1, 0, 0, &buffer_backspace_wrap);
  scm_c_define_gsubr("buffer-set-contents!", 2, 0, 0, &buffer_set_wrap);
  scm_c_define_gsubr("buffer-cursor-move!", 2, 0, 0, &buffer_cursor_wrap);
  scm_c_define_gsubr("buffer->string", 1, 0, 0, &buffer_string_wrap);
}
